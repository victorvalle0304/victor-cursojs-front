export const CONNECT = "connection";
export const CREATE_USER = "createUser";
export const SEND_MESSAGE = "sendMessage";
export const EMIT_RESPONSE = "emitResponse";
export const DISCONNECT = "disconnect";
export const LOAD_CHARACTERS = "loadCharacters";
export const PICK_CHARACTER =  "pickCharacter";
export const GUESSING_CHARACTER =  "guessingCharacter";
