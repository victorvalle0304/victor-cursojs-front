import React, { useState, useEffect } from "react"
import { Container } from "../character/Container"
import { LOAD_CHARACTERS, GUESSING_CHARACTER } from "../../common/constants/events"
import "../../styles/card/_card.css"

export function BoardContainer({ socket }) {

  const [characters, setCharacters] = useState([])

  useEffect(() => {
    socket.on(LOAD_CHARACTERS, data => {
      setCharacters(data)      
    })
  })

  function guessingCharacter (id) {
    socket.emit (GUESSING_CHARACTER, id)    
  }

  const charactersList = characters.map(({ id, name, image }) => (
    <li key={id} onClick={()=> guessingCharacter(id)}>
      <Container name={name} image={image} ></Container>
    </li>
  ))
  return (
    <div className="cards-container">
      <ul> {charactersList} </ul>
    </div>
  )
}
