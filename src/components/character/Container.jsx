import React from "react"
import { Image } from "./Image"
import { Details } from "./Details"
import "../../styles/card/_card.css"

export const Container = props => {
  const { name, image } = props
  return (
    <>
      <Image url={image} alt={name}></Image>
      <Details name={name}></Details>
    </>
  )
}
