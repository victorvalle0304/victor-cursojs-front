import React from "react"

export const Image = props => {
  const { url, alt } = props
  return <img  
  className = "card" 
  src={url} alt={alt}  >

  </img>
}
