import React, { useState, useEffect } from "react"
import { PICK_CHARACTER } from "../../common/constants/events"
import { Container } from "../character/Container"
import "../../styles/card/_card.css"

export function CharacterPicked ({socket}) {

  const [character, setCharacter] = useState({})

  useEffect(() => {
    socket.on(PICK_CHARACTER, data => {
      setCharacter(data)            
    })
  })

  return (
    <div className ="cardPicked" >
      <Container name={character.name} image={character.image}>
      </Container>
    </div>
  ) 

}