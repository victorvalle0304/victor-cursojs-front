import React from "react"
import { Messages } from "./Messages"
import { ChatSender } from "./ChatSender"
import { CharacterPicked } from "./CharacterPicked"
import "../../styles/chat/_chat.css"

export function Chat({ socket, nick }) {
  return (
    <div className="chat-container">
      <CharacterPicked socket={socket}></CharacterPicked>
      <Messages socket={socket}></Messages>
      <ChatSender socket={socket} nick={nick}></ChatSender>
    </div>
  )
}
