import React, { useState } from "react"
import { SEND_MESSAGE } from "../../common/constants/events"

export function ChatSender({ socket, nick }) {
  const [message, setMessage] = useState("")

  function handleChange(event) {
    setMessage(event.target.value)
  }

  function handleSubmit(event) {
    event.preventDefault()
    const toSend = {
      nick,
      data: message,
    }
    socket.emit(SEND_MESSAGE, JSON.stringify(toSend))
    setMessage("")
  }

  return (
    <form className="chat-form" onSubmit={handleSubmit}>
      <input
        className="chat-input"
        type="text"
        onChange={handleChange}
        value={message}
      ></input>
      <button className="chat-send" type="submit">
        Send Message
      </button>
    </form>
  )
}
