import React, { useState, useEffect, useRef } from "react"
import { EMIT_RESPONSE } from "../../common/constants/events"
import "../../styles/chat/_chat.css"

export function Messages({ socket }) {
  const $list = useRef(null)
  const [messages, setMessages] = useState([])

  useEffect(() => {
    socket.on(EMIT_RESPONSE, data => {
      const parsedMessage = JSON.parse(data)
      setMessages([...messages, parsedMessage])
    })
    $list.current.scrollTop = $list.current.scrollHeight
  })

  const messagesList = messages.map((current, index) => (
    <li key={index} className="chat-message">
      <h6>{current.nick}</h6>
      <p>{current.data}</p>
    </li>
  ))

  return (
    <ul className="chat-messages" ref={$list}>
      {messagesList}
    </ul>
  )
}
