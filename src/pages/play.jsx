import React, { useState, useEffect } from "react"
import { BoardContainer } from "../components/board/BoardContainer"
import Client from "socket.io-client"
import { Chat } from "../components/chat/ChatContainer"
import "../styles/play/_play.css"

import { CREATE_USER } from "../common/constants/events"

const PORT = 3001
const ENDPOINT = `http://localhost:${PORT}`


export default function Home() {
  const [socket, setSocket] = useState(null)
  const [nick, setNick] = useState("")  

  useEffect(() => {
    if (!socket) {
      setSocket(Client(ENDPOINT))
    }
    if (socket) {
      socket.on(CREATE_USER, data => {
        const parsedMessage = JSON.parse(data)
        setNick(parsedMessage.nick)        
      })      
    }
  }, [socket])

 

  if (!socket) {
    return <p>Loading</p>
  }
  return (
    <div className="play-page">
      <BoardContainer socket={socket} ></BoardContainer>
      <Chat socket={socket} nick={nick}></Chat>
    </div>
  )
}
